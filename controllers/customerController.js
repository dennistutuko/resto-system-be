const { Customer, OrderPemesanan } = require("../models");

module.exports = {
  listAll: async (req, res, next) => {
    try {
      await Customer.findAll(
        { order: [["id", "DESC"]] },
        { include: [{ model: OrderPemesanan, as: "order" }] }
      ).then((konsumen) => {
        res.status(200).json(konsumen);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
  listById: async (req, res, next) => {
    try {
      await Customer.findByPk(req.params.id, {
        include: [{ model: OrderPemesanan, as: "order" }],
      }).then((konsumen) => {
        if (!konsumen) {
          return res.status(400).send({ message: "Customer is not found!" });
        }
        return res.status(200).json(konsumen);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  create: async (req, res, next) => {
    try {
      const { email, nama, noHp } = req.body;
      if (!email || !nama || !noHp)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await Customer.create({
        email: email,
        nama: nama,
        noHP: noHp,
      }).then((konsumen) => {
        res.status(200).json(konsumen);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  createWithOrder: async (req, res, next) => {
    try {
      const { email, nama, noHp, noMeja } = req.body;
      if (!email || !nama || !noHp || !noMeja)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await Customer.create(
        {
          email: email,
          nama: nama,
          noHP: noHp,
          OrderPemesanan: {
            noMeja: noMeja,
          },
        },
        {
          include: [{ model: OrderPemesanan, as: "order" }],
        }
      ).then((konsumen) => {
        res.status(200).json(konsumen);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { email, nama, noHp, order } = req.body;
      if (!email || !nama || !noHp || !order)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await Customer.findByPk(req.params.id, {
        include: [{ model: OrderPemesanan, as: "order" }],
      }).then((konsumen) => {
        if (!konsumen) {
          return res.status(400).send({ message: "Customer is not found!" });
        }
        return konsumen
          .updateAttributes(
            {
              email: email || konsumen.email,
              nama: nama || konsumen.nama,
              noHP: noHp || konsumen.noHp,
              order: order || konsumen.order,
            },
            {
              include: [{ model: OrderPemesanan, as: order }],
            }
          )
          .then((konsumen) => {
            res.status(200).json(konsumen);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      await Customer.findByPk(req.params.id).then((konsumen) => {
        if (!konsumen) {
          return res.status(400).send({ message: "Customer is not found!" });
        }
        return konsumen.destroy().then((konsumen) => {
          res.status(200).json(konsumen);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
