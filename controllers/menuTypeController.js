const { Menu, MenuType } = require("../models");

module.exports = {
  listAll: async (req, res, next) => {
    try {
      await MenuType.findAll({ order: [["id", "ASC"]] }).then((kategori) => {
        res.status(200).json(kategori);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listById: async (req, res, next) => {
    try {
      await MenuType.findByPk(req.params.id, {
        include: [{ model: Menu, as: "menu" }],
      }).then((kategori) => {
        if (!kategori) {
          return res.status(400).send({ message: "Type not found!" });
        }
        return res.status(200).json(kategori);
      });
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  },

  create: async (req, res, next) => {
    try {
      const { namaType } = req.body;
      if (!namaType)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await MenuType.create({
        namaType: namaType,
      }).then((kategori) => {
        res.status(200).json(kategori);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  createWithMenu: async (req, res, next) => {
    try {
      const { namaType, menu } = req.body;
      if (!namaType || menu)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await MenuType.create(
        {
          namaType: namaType,
          menu: menu,
        },
        {
          include: [{ model: Menu, as: "menu" }],
        }
      ).then((kategori) => {
        res.status(200).json(kategori);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { namaType } = req.body;
      if (!namaType)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await MenuType.findByPk(
        req.params.id
        // {include: [{ model: Menu, as: "menu" }],}
      ).then((kategori) => {
        if (!kategori) {
          return res.status(404).send({ message: "Type is not found!" });
        }
        return kategori
          .updateAttributes(
            {
              namaType: namaType || kategori.namaType,
              // menu: menu || kategori.menu,
            }
            // {
            //   include: [{ model: Menu, as: "menu" }],
            // }
          )
          .then((kategori) => {
            res.status(200).json(kategori);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      await MenuType.findByPk(req.params.id).then((kategori) => {
        if (!kategori) {
          return res.status(400).send({ message: "Type is not found!" });
        }
        return kategori.destroy().then((kategori) => {
          res.status(200).json(kategori);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
