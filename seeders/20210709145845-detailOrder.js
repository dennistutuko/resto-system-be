"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "DetailOrders",
      [
        {
          id: 1,
          quantity: 2,
          note: "no ice",
          createdAt: new Date(),
          updatedAt: new Date(),
          menuId: 1,
          orderId: 1,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("DetailOrders", null, {});
  },
};
