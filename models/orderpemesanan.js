"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class OrderPemesanan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      OrderPemesanan.belongsTo(models.Customer, {
        foreignKey: "customerId",
      });
      OrderPemesanan.hasMany(models.DetailOrder, {
        foreignKey: "orderId",
        as: "detailOrder",
      });
    }
  }
  OrderPemesanan.init(
    {
      waktuOrder: DataTypes.DATE,
      noMeja: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "OrderPemesanan",
    }
  );
  return OrderPemesanan;
};
