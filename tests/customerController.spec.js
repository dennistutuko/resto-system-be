const customer = require("../controllers/customerController.js");
const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

describe("customer.create function", () => {
  test("res.json called with {status: true, message: Hello World}", (done) => {
    const req = mockRequest();
    const res = mockResponse();
    customer.create(req, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({ status: true, message: "Hello World!" });
    done();
  });
});
