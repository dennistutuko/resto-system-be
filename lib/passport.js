const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { UserResto } = require("../models");
// require("dotenv").config();

//Passport JWT option
const option = {
  //ngasih tau get jwt darimana
  jwtFromRequest: ExtractJwt.fromHeader("Authorization"),
  //secret yg kita punya
  secretOrKey: "secret",
};

passport.use(
  new JwtStrategy(option, async (payload, done) => {
    //tindakan selanjutnya
    // console.log(payload);
    try {
      const user = await UserResto.findByPk(payload.id);
      return done(null, user);
    } catch (error) {
      done(null, false);
    }
    // UserResto.findByPk(payload.id)
    //   .then((user) => done(null, user))
    //   .catch((err) => done(null, false, { message: err.message }));
    // })
  })
);

module.exports = passport;
