const express = require("express");
const path = require("path");
const logger = require("morgan");
const app = express();
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const cors = require("cors");

const indexRoute = require("./routes/indexRoute");

const PORT = process.env.PORT || 5000;
// const PORT = 5000;

app.use(cors());
app.use(logger("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//memanngil route
app.use("/api", indexRoute);
app.use("/", (req, res) => {
  res.send("Welcome to Resto app!");
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
