const router = require("express").Router();
const restrict = require("../middleware/restrict");
const customerController = require("../controllers/customerController");
const userRestoController = require("../controllers/userRestoController");
const menuTypeController = require("../controllers/menuTypeController");
const menuController = require("../controllers/menuController");
const detailOrderController = require("../controllers/detailOrderController");
const orderController = require("../controllers/orderController");

// tanpa menggunakan route /api

// controller /customer
//findAll
router.get("/customer", customerController.listAll);
//findOne
router.get("/customer/:id", customerController.listById);
//create
router.get("/customer/add-customer");
router.post("/customer/add-customer", customerController.create);
// //update
// router.get("/customer/edit/:id");
// router.put("/customer/edit/:id", customerController.update);
// //delete
// router.get("/customer/delete/:id");
// router.delete("/customer/delete/:id", customerController.delete);
//=======================================================================================

// controller /userResto
//findAll
router.get("/user-resto", userRestoController.listAll);
//findOne
router.get("/user-resto/:id", userRestoController.listById);
//create
router.get("/user-resto/register");
router.post("/user-resto/register", userRestoController.create);
//login
router.get("/user-resto/login");
router.post("/user-resto/login", userRestoController.authUser);
//update
router.get("/user-resto/edit-profile/:id");
router.put(
  "/user-resto/edit-profile/:id",
  restrict,
  userRestoController.update
);
//delete
router.get("/user-resto/delete/:id");
router.delete("/user-resto/delete/:id", restrict, userRestoController.delete);

//router Restoran
//add restoran
router.get("/resto/register");
router.post("/resto/register", userRestoController.createResto);
//list restoran by Id
router.get("/resto/:id", userRestoController.listResto);
//=======================================================================================

// controller /menuType
//findAll
router.get("/menu-type", menuTypeController.listAll);
//findOne
router.get("/menu-type/:id", menuTypeController.listById);
//create MenuType
router.get("/menu-type/add-type");
router.post("/menu-type/add-type", menuTypeController.create);
//update
router.get("/menu-type/edit/:id");
router.put("/menu-type/edit/:id", menuTypeController.update);
//delete
router.get("/menu-type/delete/:id");
router.delete("/menu-type/delete/:id", restrict, menuTypeController.delete);
//=======================================================================================

// controller /menu
//findAll
router.get("/menu", menuController.listAll);
//findOne
router.get("/menu/:id", menuController.listById);
//create
router.get("/menu/add-menu");
router.post("/menu/add-menu", menuController.create);
//update
router.get("/menu/edit/:id");
router.patch("/menu/edit/:id", menuController.update);
//delete
router.get("/menu/delete/:id");
router.delete("/menu/delete/:id", menuController.delete);
//=======================================================================================

// controller /detailOrder
//findAll
router.get("/detail-order", detailOrderController.listAll);
//findOne
router.get("/detail-order/:id", detailOrderController.listById);
//create
router.get("/detail-order/add-detail-order");
router.post("/detail-order/add-detail-order", detailOrderController.create);
//update
router.get("/detail-order/edit/:id");
router.put("/detail-order/edit/:id", detailOrderController.update);
//delete
router.get("/detail-order/delete/:id");
router.delete("/detail-order/delete/:id", detailOrderController.delete);
//=======================================================================================

// controller /order
//findAll
router.get("/order", orderController.listAll);
//findOne
router.get("/order/:id", orderController.listById);
//create
router.get("/order/add-order");
router.post("/order/add-order", orderController.create);
//update
router.get("/order/edit/:id");
router.put("/order/edit/:id", orderController.update);
//delete
router.get("/order/delete/:id");
router.delete("/order/delete/:id", orderController.delete);
//=======================================================================================

//Route advance addWith
//MenuType and Menu
// router.get("/menu-type/add_with_menu");
// router.post("/menu-type/add_with_menu", menuTypeController.createWithMenu);
// //Customer and OrderPemesanan
router.get("/customer/add_with_order");
router.post("/customer/add_with_order", customerController.createWithOrder);
// //OrderPemesanan and DetailOrder
// router.get("/order/add_with_detail_order");
// router.post(
//   "/order/add_with_detail_order",
//   orderController.createWithDetailOrder
// );
//Menu and DetailOrder
// router.get("/menu/add_with_detail_order");
// router.post(
//   "/menu/add_with_detail_order",
//   menuController.createWithDetailOrder
// );

module.exports = router;
