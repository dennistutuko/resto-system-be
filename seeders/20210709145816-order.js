"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "OrderPemesanans",
      [
        {
          id: 1,
          waktuOrder: new Date(),
          noMeja: "2",
          customerId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("OrderPemesanans", null, {});
  },
};
