"use strict";
const { Model } = require("sequelize");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class UserResto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserResto.belongsTo(models.Restoran, { foreignKey: "restoId" });
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        email: this.email,
      };
      const secret = "secret";
      const token = jwt.sign(payload, secret);
      return token;
    };
  }
  UserResto.init(
    {
      email: DataTypes.STRING,
      namaUser: DataTypes.STRING,
      noHpUser: DataTypes.STRING,
      password: DataTypes.STRING,
      position: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "UserResto",
    }
  );
  return UserResto;
};
