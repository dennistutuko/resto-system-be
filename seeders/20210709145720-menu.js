"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "Menus",
      [
        {
          id: 1,
          namaMenu: "nasi",
          harga: 12000,
          deskripsi: "nasi lemak",
          isAvailable: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          typeId: 1,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("Menus", null, {});
  },
};
