const { Menu, MenuType, DetailOrder } = require("../models");

module.exports = {
  listAll: async (req, res) => {
    try {
      await Menu.findAll({
        order: [["id", "ASC"]],
      }).then((menu) => {
        res.status(200).json(menu);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listById: async (req, res) => {
    try {
      const id = req.params.id;
      await Menu.findByPk(req.params.id, {
        include: [{ model: DetailOrder, as: "detailOrder" }],
      }).then((menu) => {
        return res.status(200).json(menu);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  create: async (req, res) => {
    try {
      const { namaMenu, harga, deskripsi, isAvailable, typeId } = req.body;
      if (!namaMenu || !harga || !typeId || !deskripsi || !isAvailable)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      await Menu.create({
        namaMenu: namaMenu,
        harga: harga,
        deskripsi: deskripsi,
        isAvailable: isAvailable,
        typeId: typeId,
      }).then((menu) => {
        res.status(200).json(menu);
      });
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  },

  update: async (req, res) => {
    try {
      const { namaMenu, harga, deskripsi, isAvailable, typeId } = req.body;
      if (!namaMenu || !harga || !deskripsi || !isAvailable || !typeId)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      const id = req.params.id;
      await Menu.findOne(
        { where: { id: id } },
        { include: [{ model: MenuType, as: "type" }] }
      ).then((menu) => {
        if (!menu) {
          return res.status(400).send({ message: "Menu is not found!" });
        }
        return menu
          .update({
            namaMenu: namaMenu || menu.namaMenu,
            harga: harga || menu.harga,
            deskripsi: deskripsi || menu.deskripsi,
            isAvailable: isAvailable || menu.isAvailable,
            typeId: typeId || menu.typeId,
          })
          .then((menu) => {
            res.status(200).json(menu);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res) => {
    try {
      await Menu.findByPk(req.params.id).then((menu) => {
        if (!menu) {
          return res.status(400).send({ message: "Menu is not found!" });
        }
        return menu.destroy().then((menu) => {
          res.status(200).json(menu);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
