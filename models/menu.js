"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Menu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Menu.hasMany(models.DetailOrder, {
        foreignKey: "menuId",
        as: "detailOrder",
      });
      Menu.belongsTo(models.MenuType, { foreignKey: "typeId" });
    }
  }
  Menu.init(
    {
      namaMenu: DataTypes.STRING,
      harga: DataTypes.INTEGER,
      deskripsi: DataTypes.STRING,
      isAvailable: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Menu",
    }
  );
  return Menu;
};
