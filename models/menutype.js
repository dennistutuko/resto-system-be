"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class MenuType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      MenuType.hasMany(models.Menu, { foreignKey: "typeId", as: "menu" });
    }
  }
  MenuType.init(
    {
      namaType: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "MenuType",
    }
  );
  return MenuType;
};
