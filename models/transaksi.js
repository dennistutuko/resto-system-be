"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Transaksi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Transaksi.belongsTo(models.OrderPemesanan, {
      //   foreignKey: "id",
      //   as: "orderId",
      // });
    }
  }
  Transaksi.init(
    {
      totalharga: DataTypes.DECIMAL,
    },
    {
      sequelize,
      modelName: "Transaksi",
    }
  );
  return Transaksi;
};
