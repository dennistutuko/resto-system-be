const { OrderPemesanan, DetailOrder, Menu } = require("../models");

module.exports = {
  listAll: async (req, res, next) => {
    try {
      await DetailOrder.findAll({
        order: [["id", "ASC"]],
      }).then((detail) => {
        res.status(200).json(detail);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listById: async (req, res) => {
    try {
      const id = req.params.id;
      await DetailOrder.findByPk(
        { where: { id: id } }
        // { include: [{ model: Menu, as: "Menu" }] }
      ).then((detail) => {
        res.status(200).json(detail);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  create: async (req, res, next) => {
    try {
      const { quantity, note, menuId, orderId } = req.body;
      // if (!quantity || !note || !menuId)
      //   return res.status(400).json("Mohon field jangan dikosongkan...");

      await DetailOrder.create({
        quantity: quantity,
        note: note,
        menuId: menuId,
        orderId: orderId,
      }).then((detail) => {
        res.status(200).json(detail);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { quantity, note, menuId, orderId } = req.body;
      if (!quantity || !note || !menuId)
        return res.status(400).json("Mohon field jangan dikosongkan...");

      await DetailOrder.findByPk(req.params.id, {
        include: [
          {
            model: Menu,
            as: "menu",
          },
          // {
          //   model: OrderPemesanan,
          //   as: "order",
          // },
        ],
      }).then((detail) => {
        if (!detail) {
          return res
            .status(400)
            .send({ message: "Detail order menu is not found" });
        }
        detail
          .update({
            quantity: quantity || detail.quantity,
            note: note || detail.note,
            menuId: menuId || detail.menuId,
            // orderId: orderId || detail.orderId,
          })
          .then((order) => {
            res.status(200).json(order);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      await DetailOrder.findByPk(req.params.id).then((detail) => {
        detail.destroy().then((detail) => {
          res.status(200).json(detail);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
