"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "MenuTypes",
      [
        {
          id: 1,
          namaType: "makanan",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          namaType: "minuman",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("MenuTypes", null, {});
  },
};
