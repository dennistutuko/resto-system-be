const { UserResto, Restoran } = require("../models");

const bcrypt = require("bcrypt");

jwtResponsesFormatter = (UserResto) => {
  let format = {
    // id: UserResto.id,
    // email: UserResto.email,
    accessToken: UserResto.generateToken(),
  };
  return format;
};

module.exports = {
  listAll: async (req, res, next) => {
    try {
      await UserResto.findAll({
        order: [["id", "DESC"]],
      }).then((user) => {
        res.status(200).json(user);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listById: async (req, res, next) => {
    try {
      const id = req.params.id;
      await UserResto.findOne({
        where: { id: id },
      }).then((user) => {
        res.status(200).json(user);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  create: async (req, res, next) => {
    try {
      const { email, namaUser, noHpUser, password, restoId } = req.body;
      if (!email || !namaUser || !noHpUser || !password)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      const encryptPassword = await bcrypt.hash(password, 10);

      await UserResto.create({
        email: email,
        namaUser: namaUser,
        noHpUser: noHpUser,
        password: encryptPassword,
        restoId: restoId,
      }).then((user) => {
        res.status(200).json(user);
      });
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  },

  authUser: async (req, res) => {
    try {
      const { email, password } = req.body;
      const correctPassword = (inputPassword, password) => {
        return new Promise((resolve) => {
          bcrypt.compare(inputPassword, password, (err, res) => {
            resolve(res);
          });
        });
      };
      const user = await UserResto.findOne({ where: { email } });
      const authentication = await correctPassword(password, user.password);
      if (!authentication) {
        return Promise.reject("Email / password is incorrect");
      }
      res.status(200).json(jwtResponsesFormatter(user));
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { email, namaUser, noHpUser, password, restoId } = req.body;
      if (!email || !namaUser || !noHpUser || !password)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });
      const id = req.params.id;
      await UserResto.findOne({
        where: { id: id },
      }).then((user) => {
        user
          .update({
            email: email || user.email,
            namaUser: namaUser || user.namaUser,
            noHpUser: noHpUser || user.noHpUser,
            password: password || user.password,
            restoId: restoId || user.restoId,
          })
          .then((user) => {
            res.status(200).json(user);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      await UserResto.findByPk(req.params.id).then((user) => {
        user.destroy().then((user) => {
          res.status(200).json(user);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  createResto: async (req, res) => {
    try {
      const { namaResto, alamatResto, emailResto, noHpResto } = req.body;
      if (!namaResto || !alamatResto || !emailResto || !noHpResto)
        return res.status(400).json({
          result: "FAILED",
          message: "Data tidak boleh ada yang kosong",
        });

      await Restoran.create({
        namaResto: namaResto,
        alamatResto: alamatResto,
        emailResto: emailResto,
        noHpResto: noHpResto,
      }).then((resto) => {
        res.status(200).json(resto);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listResto: async (req, res) => {
    try {
      const id = req.params.id;
      await Restoran.findOne(
        {
          where: { id: id },
        },
        { include: [{ model: UserResto, as: "employee" }] }
      ).then((resto) => {
        res.status(200).json(resto);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
