const { OrderPemesanan, Customer, DetailOrder } = require("../models");

module.exports = {
  listAll: async (req, res) => {
    try {
      await OrderPemesanan.findAll({
        order: [["id", "ASC"]],
      }).then((order) => {
        res.status(200).json(order);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  listById: async (req, res, next) => {
    try {
      await OrderPemesanan.findByPk(req.params.id, {
        include: [{ model: DetailOrder, as: "detailOrder" }],
      }).then((order) => {
        if (!order) {
          return res.status(400).send({ message: "Order is not found!" });
        }
        return res.status(200).json(order);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  create: async (req, res, next) => {
    try {
      const { waktuOrder, noMeja, customerId } = req.body;
      if (!noMeja || !customerId)
        return res.status(400).json("Mohon field jangan dikosongkan...");

      await OrderPemesanan.create({
        waktuOrder: waktuOrder,
        noMeja: noMeja,
        customerId: customerId,
      }).then((order) => {
        res.status(200).json(order);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  createWithDetailOrder: async (req, res, next) => {
    try {
      const { waktuOrder, noMeja, detailOrder } = req.body;
      if (!waktuOrder || !noMeja || !detailOrder)
        return res.status(400).json("Mohon field jangan dikosongkan...");

      await OrderPemesanan.create(
        {
          waktuOrder: waktuOrder,
          noMeja: noMeja,
          detailOrder: detailOrder,
        },
        {
          include: [{ model: DetailOrder, as: "detailOrder" }],
        }
      ).then((order) => {
        res.status(200).json(order);
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { waktuOrder, noMeja, detailOrder } = req.body;
      if (!waktuOrder || !noMeja || !detailOrder)
        return res.status(400).json("Mohon field jangan dikosongkan...");

      await OrderPemesanan.findByPK(req.params.id, {
        include: [
          {
            model: Customer,
            as: "customer",
          },
          {
            model: DetailOrder,
            as: "detailOrder",
          },
        ],
      }).then((order) => {
        order
          .update(
            {
              waktuOrder: waktuOrder || order.waktuOrder,
              noMeja: noMeja || order.noMeja,
              detailOrder: detailOrder || order.detailOrder,
            },
            {
              include: [{ model: DetailOrder, as: "detailOrder" }],
            }
          )
          .then((order) => {
            res.status(200).json(order);
          });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      await OrderPemesanan.findByPk(req.params.id).then((order) => {
        if (!order) {
          return res.status(400).send({ message: "Order is not found!" });
        }
        return order.destroy().then((order) => {
          res.status(200).json(order);
        });
      });
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
