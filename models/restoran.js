"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Restoran extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Restoran.hasMany(models.UserResto, {
        foreignKey: "restoId",
        as: "restoran",
      });
    }
  }
  Restoran.init(
    {
      namaResto: DataTypes.STRING,
      alamatResto: DataTypes.STRING,
      emailResto: DataTypes.STRING,
      noHpResto: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Restoran",
    }
  );
  return Restoran;
};
